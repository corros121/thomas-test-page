// Map all constants here for convenience in updating values in the future...

const ROUTES = Object.freeze({
  AUTH: {
    LOGIN: '#'
  }
})

export {
  ROUTES
}

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Lato', ...defaultTheme.fontFamily.sans]
      },
      colors: {
        blue: {
          light: '#BEE3F8',
          DEFAULT: '#3182CE',
          darker: '#2B6CB0',
          darkest: '#2C5282'
        },
        green: {
          100: '#E6FFFA',
          200: '#B2F5EA',
          300: '#81E6D9',
          400: '#38B2AC',
          500: '#319795',
          600: '#2C7A7B',
          700: '#285E61'
        },
        gray: {
          lightest: '#EBF4FF',
          lighter: '#CBD5E0',
          light: '#718096',
          DEFAULT: '#4A5568',
          darker: '#2D3748'
        },
        flesh: '#F7FAFC'
      },
      fontSize: {
        display: [
          '130px',
          {
            letterSpacing: '0px',
            lineHeight: '156px'
          }
        ],
        h1: [
          '42px',
          {
            letterSpacing: '1.26px',
            lineHeight: '50px'
          }
        ],
        h2: [
          '21px',
          {
            letterSpacing: '0px',
            lineHeight: '25px'
          }
        ],
        h3: [
          '16px',
          {
            letterSpacing: '0.47px',
            lineHeight: '19px'
          }
        ],
        body: [
          '14px',
          {
            letterSpacing: '0.84px',
            lineHeight: '17px'
          }
        ]
      },
      boxShadow: {
        header: '0px 3px 6px #00000029;',
        'header-top': '0px -1px 3px #00000033'
      },
      height: {
        1.25: '5px',
        100: '25rem'
      },
      zIndex: {
        '-1': '-1'
      }
    }
  },
  variants: {
    extend: {
      borderColor: ['active', 'focus'],
      borderWidth: ['active', 'focus']
    }
  },
  plugins: []
}
